# Importing the required libraries 
from __future__ import division
import cv2
import time
import pandas
import os
import csv

os.getcwd()

# Creating a function to compare the two images. 
def compare_image(img1, img2):
    original = cv2.imread(img1)
    image_to_compare = cv2.imread(img2)

    start = time.time()
    sift = cv2.xfeatures2d.SIFT_create()
    kp_1, desc_1 = sift.detectAndCompute(original, None)
    kp_2, desc_2 = sift.detectAndCompute(image_to_compare, None)

    index_params = dict(algorithm=0, trees=5)
    search_params = dict()
    flann = cv2.FlannBasedMatcher(index_params, search_params)

    matches = flann.knnMatch(desc_1, desc_2, k=2)
    good_points = []
    for m, n in matches:
        if m.distance < 0.6*n.distance:
            good_points.append(m)

        number_keypoints = 0
        if len(kp_1) <= len(kp_2):
            number_keypoints = len(kp_1)
        else:
            number_keypoints = len(kp_2)

    # Calculate score
    end = time.time()
    similar = round(1 - len(good_points) / number_keypoints, 2)
    elapsed = round(end - start, 3)

    return {
        'elapsed': str(elapsed),
        'similar': str(similar)
    }


# Getting the current time to append it to the exported csv filename. 
current_time = time.strftime("%m.%d.%y.%H:%M", time.localtime())
output_name = 'output/compared-images-%s.csv' % current_time

# Creating the final CSV with the result and using the above function.
with open(output_name, 'w+') as csvfile:
    fieldnames = ['image1', 'image2', 'similar', 'elapsed']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    writer.writeheader()
# Using the input file to have the images path.     
    with open('input/compare_images.csv', 'r+') as srcFile:
        reader = csv.DictReader(srcFile)
        for row in reader:
            result = compare_image(row['image1'], row['image2'])
            writer.writerow({ 'image1': row['image1'], 'image2': row['image2'], 'similar': result['similar'], 'elapsed': result['elapsed'] })
