# compare-images

compare-images is a python tool to compare two different images and generates a csv report with the comparison. 


## Usage

In order to use this, update input/compare_images.csv to add the two images you need to compare under image1 & image2. 
After updating input/compare_images.csv, add the same images to the images directory. 

Once, you push your changes, it will trigger a job. Once, the job is compelted, you can download the output by downloading the artifacts.

